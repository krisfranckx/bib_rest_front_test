import { Component } from '@angular/core';
import { UserService } from '../../service/user.service';
import { User } from '../../interface/user';
import { NgFor, NgIf } from '@angular/common';

@Component({
  selector: 'app-users',
  standalone: true,
  imports: [NgFor, NgIf],
  templateUrl: './users.component.html',
  styleUrl: './users.component.css'
})
export class UsersComponent {
  users: User[]=[];
  constructor(private userService:UserService){}

  ngOnInit(): void {
    this.users=[];
    this.userService.getUsers().subscribe(
      (response) => response.data.users!.forEach(user=>this.users.push(user as User))
    ); 
  }

}
