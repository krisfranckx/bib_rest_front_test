import { Component } from '@angular/core';
import { NgFor, NgIf } from '@angular/common';
import { UserService } from '../../service/user.service';
import { User } from '../../interface/user';


@Component({
  selector: 'app-user-profile',
  standalone: true,
  imports: [NgFor, NgIf],
  templateUrl: './user-profile.component.html',
  styleUrl: './user-profile.component.css'
})
export class UserProfileComponent {
  user!: User;
  id:number=1;
  constructor(private userService:UserService){}

  ngOnInit(): void {
    this.userService.getUser(this.id).subscribe(
      (response) => this.user=response.data.user as User
    ); 
  }
}