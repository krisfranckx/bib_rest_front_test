import { NgFor, NgIf } from '@angular/common';
import { Component } from '@angular/core';
import { BibItemService } from '../../service/bib-item.service';
import { Magazine } from '../../interface/magazine';
import { Book } from '../../interface/book';

@Component({
  selector: 'app-list',
  standalone: true,
  imports: [NgFor, NgIf],
  templateUrl: './list.component.html',
  styleUrl: './list.component.css'
})
export class ListComponent {

  bookItems: Book[] = [];
  magazineItems: Magazine[] = [];


  constructor(private bibItemService : BibItemService){  }  
  
  ngOnInit(): void {
    this.bookItems=[];
    this.magazineItems=[];
    this.bibItemService.getBibItems().subscribe(
      (response) => response.data.bibItems!.forEach(
          (item)=>{
            if (item.type==='book'){this.bookItems.push(item as Book)}
            else{ this.magazineItems.push(item as Magazine)}
          }
      )
    ); 
  }
}
