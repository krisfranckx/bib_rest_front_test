import { Component, OnInit } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { BibItemService } from './service/bib-item.service';
import { NgFor, NgIf } from '@angular/common';
import { Book } from './interface/book';
import { Magazine } from './interface/magazine';
import { CustomNavbarComponent } from './component/custom-navbar/custom-navbar.component';
import { ListComponent } from './component/list/list.component';
import { UsersComponent } from './component/users/users.component';
import { UserProfileComponent } from './component/user-profile/user-profile.component';


@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, NgFor, NgIf, CustomNavbarComponent, ListComponent, UsersComponent, UserProfileComponent],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})

export class AppComponent{
[x: string]: any;  
}



