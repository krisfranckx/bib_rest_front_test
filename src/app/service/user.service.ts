import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, catchError, throwError } from 'rxjs';
import { CustomResponse } from '../interface/custom-response';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private readonly apiUrl= 'http://localhost:8080';

  constructor(private http:HttpClient) { }

  public getUsers(): Observable<CustomResponse>{
    return this.http.get<CustomResponse>(`${this.apiUrl}/admin/users`);
  }
  public getUser(id:number): Observable<CustomResponse>{
    return this.http.get<CustomResponse>(`${this.apiUrl}/admin/users/`+id);
  }
}
