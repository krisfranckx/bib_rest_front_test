
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, catchError, throwError } from 'rxjs';
import { CustomResponse } from '../interface/custom-response';

@Injectable({providedIn: 'root'})
export class BibItemService {
 
  private readonly apiUrl= 'http://localhost:8080';

  constructor(private http: HttpClient) { }


  public getBibItems(): Observable<CustomResponse>{
    return this.http.get<CustomResponse>(`${this.apiUrl}/admin/items`);
  }

}



