import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { HttpClientModule } from "@angular/common/http";
import { BibItemService } from "./service/bib-item.service";

@NgModule({
    declarations: [
    ],
    imports: [
        BrowserModule,
        HttpClientModule
    ],
    providers:[BibItemService]
})
export class AppModule {
    
}