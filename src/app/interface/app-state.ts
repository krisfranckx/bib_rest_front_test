import { DataState } from "../enum/date-state.enum";

export interface AppState<T>{
    dataState: DataState;
    appData?: T;
    error?: string;
}