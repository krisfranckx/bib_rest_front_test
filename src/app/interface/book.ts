import { Taal } from "../enum/taal.enum";
import { BibItem } from "./bibItem";

export interface Book extends BibItem{
    bookId : number;
    auteur : string;
    taal : Taal;
    graad : string;
    fictieNonFictie : string;
}