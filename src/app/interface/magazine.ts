import { BibItem } from "./bibItem";

export interface Magazine extends BibItem{
    magazineId : number;
    nummer : number;
    jaargang : number;
}