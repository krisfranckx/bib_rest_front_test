import { BibItem } from "./bibItem";
import { Book } from "./book";
import { Magazine } from "./magazine";
import { User } from "./user";

export interface CustomResponse{
    timeStamp : Date;
    statusCode : number;
    status : string;
    reason : string; //reason for error
    message : string; //succesMessage voor frontend
    developerMessage? : string;  //
    data: {
        bibItems?: (Book|Magazine)[],
        bibItem?: BibItem|Book|Magazine,
        users?: User[],
        user?:User
    };
}