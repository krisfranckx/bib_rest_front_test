export enum Taal{
    Algemeen = 'algemeen',
    Engels ='Engels',
    Nederlands='Nederlands',
    Frans='Frans',
    Spaans = 'Spaans',
    Duits='Duits'
}